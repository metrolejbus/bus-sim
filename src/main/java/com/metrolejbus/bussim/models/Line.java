package com.metrolejbus.bussim.models;

import java.util.List;
import java.util.Set;


public class Line {
    public Long id;
    public String name;
    public String type;
    public Set<Station> stations;
    public List<LineSchedule> schedules;
    public String route;
}
