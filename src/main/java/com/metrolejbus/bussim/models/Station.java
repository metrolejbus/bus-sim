package com.metrolejbus.bussim.models;

public class Station {
    public Long id;
    public String name;
    public Zone zone;
    public Location location;
}
