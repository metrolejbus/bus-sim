package com.metrolejbus.bussim.models;

import java.util.List;


public class Journey {
    public Line line;
    public List<JourneyStep> steps;
}
