package com.metrolejbus.bussim.models;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;


public class LineSchedule {
    public long id;
    public List<DayOfWeek> appliesTo;
    public List<LocalTime> routeA;
    public List<LocalTime> routeB;
    public ValidityPeriod validityPeriod;

    // TODO: remove
    public boolean active;


    public boolean isValid() {
        LocalDate today = LocalDate.now();
        LocalDate startDate = validityPeriod.startDate;
        LocalDate endDate = validityPeriod.endDate;

        if (!startDate.isBefore(today) && !startDate.isEqual(today)) {
            return false;
        }

        return endDate.isAfter(today) || endDate.isEqual(today);
    }

    public boolean isApplicableTo(DayOfWeek today) {
        return appliesTo.contains(today);
    }
}
