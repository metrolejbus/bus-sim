package com.metrolejbus.bussim.models;

import java.time.LocalDate;


public class ValidityPeriod {
    public LocalDate startDate;
    public LocalDate endDate;
}
