package com.metrolejbus.bussim.simulation;

import com.metrolejbus.bussim.RestClient;
import com.metrolejbus.bussim.models.Journey;
import org.apache.catalina.core.ApplicationContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalTime;


@Service
public class Dispatcher {

    private RestClient client;
    private Schedule schedule;

    public static boolean enabled = false;


    @Autowired
    public Dispatcher(RestClient client, Schedule schedule) {
        this.client = client;
        this.schedule = schedule;
    }

    @Scheduled(cron = "0 * * * * *")
    public void run() {
        if (enabled) {
            LocalTime now = LocalTime.now();
            int time = now.getHour() * 60 + now.getMinute();
            this.dispatch(schedule.nextBatch(time));
        }
    }

    private void dispatch(Schedule.Batch batch) {
        for (Journey journey: batch.journeys) {
            System.out.println("Dispatching vehicle");
            Vehicle vehicle = new Vehicle();
            vehicle.setDependencies(journey, client);
            new Thread(vehicle).start();
        }
    }
}
