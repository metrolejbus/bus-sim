package com.metrolejbus.bussim.simulation;

import com.metrolejbus.bussim.RestClient;
import com.metrolejbus.bussim.models.Journey;
import com.metrolejbus.bussim.models.JourneyStep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


@Service
public class Vehicle implements Runnable {

    private Journey journey;
    private RestClient client;

    void setDependencies(Journey journey, RestClient client) {
        this.client = client;
        this.journey = journey;
    }

    @Override
    public void run() {
        String timestamp = LocalDateTime.now()
            .format(DateTimeFormatter.ISO_DATE_TIME);

        long journeyId = client.createJourney(journey.line.id, timestamp);

        for (JourneyStep step: journey.steps) {
            timestamp = LocalDateTime.now()
                .format(DateTimeFormatter.ISO_DATE_TIME);

            System.out.println("[" + timestamp.substring(11, 16) + "]: Journey " + journeyId +" [" + step.location.lat + "," + step.location.lng + "]");

            client.reportLocation(
                journeyId,
                journey.line.id,
                step.location.lat,
                step.location.lng,
                timestamp
            );

            try {
                Thread.sleep((long) (step.duration * 1000));
            } catch (InterruptedException e){
                e.printStackTrace();
            }
        }
        timestamp = LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME);

        client.reportJourneyEnd(journeyId, timestamp);
    }
}
