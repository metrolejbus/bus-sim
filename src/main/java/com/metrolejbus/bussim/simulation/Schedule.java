package com.metrolejbus.bussim.simulation;

import com.metrolejbus.bussim.models.Journey;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

@Component
public class Schedule {

    public static List<Batch> batches;

    public Schedule() {
        init();
    }

    Batch nextBatch(int time) {
        return batches.get(time);
    }

    public void addToBatch(int index, Journey journey) {
        batches.get(index).journeys.add(journey);
    }

    public void clear() {
        init();
    }

    private void init() {
        batches = new LinkedList<>();
        // For every minute of the day
        for (int i = 0; i < 24 * 60; i++) {
            batches.add(new Batch());
        }
    }

    class Batch {
        List<Journey> journeys;

        Batch() {
            journeys = new LinkedList<>();
        }
    }
}
