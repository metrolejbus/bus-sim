package com.metrolejbus.bussim;

import com.metrolejbus.bussim.models.Line;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class RestClient {

    private WebClient client;
    private static String token;

    @Autowired
    public RestClient(WebClient client) {
        this.client = client;
    }

    public void authenticate(String username, String password) {
        Map<String, String> reqBody = new HashMap<>();
        reqBody.put("username", username);
        reqBody.put("password", password);

        Map<String, Object> response = client
            .post()
            .uri("/auth/sign-in")
            .body(BodyInserters.fromObject(reqBody))
            .retrieve()
            .bodyToMono(new ParameterizedTypeReference<Map<String, Object>>() {})
            .block();

        token = (String)response.get("token");
    }

    public List<Line> getLines() {
        return client
            .get()
            .uri("/lines")
            .retrieve()
            .bodyToMono(new ParameterizedTypeReference<List<Line>>() {})
            .block();
    }

    public String getInstructions(String request) {
        return client
            .get()
            .uri(request)
            .retrieve()
            .bodyToMono(String.class)
            .block();
    }

    public long createJourney(long lineId, String departureTime) {
        Map<String, Object> reqBody = new HashMap<>();
        reqBody.put("lineId", lineId);
        reqBody.put("departureTime", departureTime);

        Map<String, Object> response = client
            .post()
            .uri("/journeys")
            .header("Authorization", "Bearer " + token)
            .body(BodyInserters.fromObject(reqBody))
            .retrieve()
            .bodyToMono(new ParameterizedTypeReference<Map<String, Object>>() {})
            .block();
        return (int)response.get("id");
    }

    public void reportLocation(long journeyId, long lineId, float lat, float lng, String timestamp) {
        Map<String, Object> reqBody = new HashMap<>();
        reqBody.put("lineId", lineId);
        reqBody.put("lat", lat);
        reqBody.put("lng", lng);
        reqBody.put("timestamp", timestamp);

        client
            .put()
            .uri("/journeys/"+ journeyId + "/location")
            .header("Authorization", "Bearer " + token)
            .body(BodyInserters.fromObject(reqBody))
            .exchange()
            .block();
    }

    public void reportJourneyEnd(long journeyId, String timestamp) {
        Map<String, Object> reqBody = new HashMap<>();
        reqBody.put("arrivalTime", timestamp);

        client
            .put()
            .uri("/journeys/"+ journeyId)
            .header("Authorization", "Bearer " + token)
            .body(BodyInserters.fromObject(reqBody))
            .exchange()
            .block();
    }
}
