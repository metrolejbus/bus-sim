package com.metrolejbus.bussim.services;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.metrolejbus.bussim.models.JourneyStep;
import com.metrolejbus.bussim.models.Location;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;


@Service
public class RoutingService {

    @Value("${routing.url}")
    private String baseUrl = "";
    private final ObjectMapper json;

    @Autowired
    public RoutingService(ObjectMapper json) {
        this.json = json;
    }

    public String prepareRequest(String route) {
        String[] points = route.split(";");

        String coordinates = Arrays.stream(points)
            .map(p -> p.substring(0, p.length() - 2))
            .map(p -> {
                String[] tokens = p.split(",");
                return String.join(",", tokens[1], tokens[0]);
            })
            .collect(Collectors.joining(";"));
        return String.join("", baseUrl, coordinates, "?overview=false&steps=true");
    }

    public List<JourneyStep> processResponse(String response) throws IOException {
        JsonNode tree = json.readTree(response);
        ArrayNode legs = (ArrayNode)tree.findValue("legs");

        List<JourneyStep> journey = new LinkedList<>();

        for (JsonNode leg: legs) {
            for (JsonNode step: leg.get("steps")) {
                float duration = step.get("duration").floatValue();

                ArrayNode intersections = (ArrayNode)step.get("intersections");
                float timePerStep = duration / intersections.size();

                for(JsonNode intersection: intersections) {
                    JourneyStep s = new JourneyStep();
                    s.duration = timePerStep;

                    float lng = intersection.get("location").get(0).floatValue();
                    float lat = intersection.get("location").get(1).floatValue();

                    s.location = new Location();
                    s.location.lat = lat;
                    s.location.lng = lng;

                    journey.add(s);
                }
            }
        }

        return journey;
    }
}
