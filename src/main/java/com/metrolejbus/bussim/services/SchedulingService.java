package com.metrolejbus.bussim.services;

import com.metrolejbus.bussim.RestClient;
import com.metrolejbus.bussim.models.Journey;
import com.metrolejbus.bussim.models.JourneyStep;
import com.metrolejbus.bussim.models.Line;
import com.metrolejbus.bussim.models.LineSchedule;
import com.metrolejbus.bussim.simulation.Schedule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;


@Service
public class SchedulingService {

    @Value("${admin.username}")
    private String username;

    @Value("${admin.password}")
    private String password;

    private RoutingService routingService;
    private RestClient client;
    private Schedule simulationSchedule;

    @Autowired
    public SchedulingService(
        RoutingService routingService,
        RestClient client,
        Schedule schedule
    ) {
        this.routingService = routingService;
        this.client = client;
        this.simulationSchedule = schedule;
    }

    @Scheduled(cron = "0 0 0 * * *")
    public void run() throws IOException {
        client.authenticate(username, password);

        List<Line> lines = client.getLines();

        simulationSchedule.clear();

        DayOfWeek today = DayOfWeek.from(LocalDate.now());
        for (Line line: lines) {
            String request = routingService.prepareRequest(line.route);
            String response = client.getInstructions(request);
            List<JourneyStep> steps = routingService.processResponse(response);

            for (LineSchedule schedule: line.schedules) {
                if (!schedule.isValid() || !schedule.isApplicableTo(today)) {
                    continue;
                }

                for (LocalTime time: schedule.routeA) {
                    Journey j = new Journey();
                    j.line = line;
                    j.steps = steps;

                    int index = time.getHour() * 60 + time.getMinute();
                    simulationSchedule.addToBatch(index, j);
                }

                List<JourneyStep> reversedSteps = new LinkedList<>(steps);
                Collections.reverse(reversedSteps);

                for (LocalTime time: schedule.routeB) {
                    Journey j = new Journey();
                    j.steps = reversedSteps;
                    j.line = line;

                    int index = time.getHour() * 60 + time.getMinute();
                    simulationSchedule.addToBatch(index, j);
                }

            }
        }
    }

}
