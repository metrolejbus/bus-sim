package com.metrolejbus.bussim;

import com.metrolejbus.bussim.services.SchedulingService;
import com.metrolejbus.bussim.simulation.Dispatcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.reactive.function.client.WebClient;

import javax.annotation.PostConstruct;
import java.io.IOException;


@SpringBootApplication
@EnableAsync
@EnableScheduling
public class BusSimApplication {

	@Bean
	public WebClient webClient() {
		return WebClient.builder()
			.baseUrl("http://localhost:8080")
			.defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
			.build();
	}

	@Autowired
	private SchedulingService service;

	@PostConstruct
	public void init() throws IOException {
		service.run();
		Dispatcher.enabled = true;
	}

	public static void main(String[] args) {
		SpringApplication.run(BusSimApplication.class, args);
	}

}

